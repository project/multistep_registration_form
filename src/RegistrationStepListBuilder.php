<?php

namespace Drupal\multistep_registration_form;

use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of registration steps entities.
 */
class RegistrationStepListBuilder extends DraggableListBuilder {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'registration_steps_admin_overview_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    $header['id'] = $this->t('Machine name');
    $header['next_button_label'] = $this->t('Next button');
    $header['previous_button_label'] = $this->t('Previous button');
    $header['user_fields'] = $this->t('User fields');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = [];
    $row['label'] = $entity->label();
    $row['id'] = ['#plain_text' => $entity->id()];
    $row['next_button_label'] = ['#plain_text' => $entity->getNextButtonLabel()];
    $row['previous_button_label'] = ['#plain_text' => $entity->getPreviousButtonLabel()];
    $row['user_fields'] = [
      '#theme' => 'item_list',
      '#items' => $entity->getUserFields(),
      '#empty' => $this->t('No user fields.'),
      '#context' => ['list_style' => 'list'],
    ];

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);

    return $operations;
  }

}

<?php

/**
 * @file
 * Provides tweaks functions for Multistep Registration Form.
 */

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\field\FieldConfigInterface;

/**
 * Return a list of user fields definitions.
 *
 * @return int[]|string[]
 */
function _multistep_registration_fome_get_user_fields() {
  $core_user_fields = [
    'name',
    'mail',
    'pass',
    'timezone',
  ];
  $user_field_definitions = Drupal::service('entity_field.manager')->getFieldDefinitions('user', 'user');
  $user_fields = array_flip(
      array_merge($core_user_fields, array_keys(
      array_filter(
          $user_field_definitions,
          function ($fieldDefinition) {
            return $fieldDefinition instanceof FieldConfigInterface;
          }
      ))));

  foreach ($user_fields as $field_name => $value) {
    if ($user_field_definitions[$field_name]->getLabel() instanceof TranslatableMarkup) {
      $user_fields[$field_name] = $user_field_definitions[$field_name]->getLabel()->render();
    } else {
      $user_fields[$field_name] = $user_field_definitions[$field_name]->getLabel();
    }
  }

  return $user_fields;
}

function _multistep_registration_form_get_used_fields() {
  $registration_step_fields = \Drupal::entityTypeManager()
    ->getStorage('registration_step')
    ->loadMultiple();

  $used_fields = [];

  foreach ($registration_step_fields as $step_id => $step) {
    $used_fields[$step_id] = $step->getUserfields();
  }

  return$used_fields;
}

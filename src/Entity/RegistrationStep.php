<?php

namespace Drupal\multistep_registration_form\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\Annotation\ConfigEntityType;
use Drupal\multistep_registration_form\RegistrationStepInterface;

/**
 * Defines the Registration Step entity.
 *
 * @ConfigEntityType(
 *   id = "registration_step",
 *   label = @Translation("Registration Step"),
 *   handlers = {
 *     "list_builder" = "Drupal\multistep_registration_form\RegistrationStepListBuilder",
 *     "form" = {
 *       "step_fields" = "Drupal\multistep_registration_form\Form\RegistrationStepFieldsForm",
 *       "add" = "Drupal\multistep_registration_form\Form\RegistrationStepForm",
 *       "edit" = "Drupal\multistep_registration_form\Form\RegistrationStepForm",
 *       "delete" = "Drupal\multistep_registration_form\Form\RegistrationStepDeleteForm",
 *     }
 *   },
 *   config_prefix = "registration_step",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "label" = "label",
 *     "user_fields" = "user_fields",
 *     "description" = "description",
 *     "next_button_label" = "next_button_label",
 *     "previous_button_label" = "previous_button_label",
 *     "weight" = "weight"
 *   },
 *   config_export = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "label" = "label",
 *     "user_fields" = "user_fields",
 *     "description" = "description",
 *     "next_button_label" = "next_button_label",
 *     "previous_button_label" = "previous_button_label",
 *     "weight" = "weight"
 *   },
 *   links = {
 *     "user-fields-form" = "/admin/config/multistep-registration-form/registration-steps/{registration_step}/fields",
 *     "edit-form" = "/admin/config/system/multistep-registration-form/registration-steps/{registration_step}",
 *     "delete-form" = "/admin/config/system/multistep-registration-form/registration-steps/{registration_step}/delete",
 *     "collection" = "/admin/config/system/multistep-registration-form/registration-steps",
 *   }
 * )
 */
class RegistrationStep extends ConfigEntityBase implements RegistrationStepInterface {

  /**
   * The Registration Step ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Registration Step label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Registration Step label.
   *
   * @var string
   */
  protected $description;

  /**
   * The Registration Step next button label.
   *
   * @var string
   */
  protected $next_button_label;

  /**
   * The Registration Step previous button label.
   *
   * @var string
   */
  protected $previous_button_label;

  /**
   * The Registration Step weight.
   *
   * @var int
   */
  protected $weight = 0;

  /**
   * The Registration Step user fields.
   *
   * @var array
   */
  protected $user_fields;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritDoc}
   */
  public function getNextButtonLabel() {
    return $this->next_button_label;
  }

  /**
   * {@inheritDoc}
   */
  public function getPreviousButtonLabel() {
    return $this->previous_button_label;
  }

  /**
   * {@inheritDoc}
   */
  public function getweight() {
    return $this->weight;
  }

  /**
   * @inheritDoc
   */
  public function getUserFields() {
    return $this->user_fields;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    $this->weight = $weight;
    return $this;
  }

}

<?php

namespace Drupal\multistep_registration_form;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a Registration Step entity.
 */
interface RegistrationStepInterface extends ConfigEntityInterface {

  /**
   * Gets the description.
   *
   * @return string
   *   The description of this registration step.
   */
  public function getDescription();

  /**
   * Gets the next button label.
   *
   * @return string
   *   The next button label of this registration step.
   */
  public function getNextButtonLabel();

  /**
   * Gets the previous button label.
   *
   * @return string
   *   The previous button label of this registration step.
   */
  public function getPreviousButtonLabel();

  /**
   * Returns the weight among registration step with the same depth.
   *
   * @return int
   *   The registration step weight.
   */
  public function getWeight();

  /**
   * Sets the weight among registration step with the same depth.
   *
   * @param int $weight
   *   The registration step weight.
   *
   * @return $this
   *   The called registration step entity.
   */
  public function setWeight($weight);

  /**
   * Gets the user fields.
   *
   * @return array
   *   The user fields of this registration step.
   */
  public function getUserFields();
}

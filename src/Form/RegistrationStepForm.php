<?php

namespace Drupal\multistep_registration_form\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for the Registration Step add and edit forms.
 */
class RegistrationStepForm extends EntityForm {

  /**
   * The entity being used by this form.
   *
   * @var \Drupal\multistep_registration_form\RegistrationStepInterface
   */
  protected $entity;

  /**
   * Constructs the RegistrationStepForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entityTypeManager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\multistep_registration_form\RegistrationStepInterface $registration_step */
    $registration_step = $this->entity;

    // To make the fieldset collapsible.
    $form['step_settings'] = [
        '#type' => 'details',
        '#title' => t('Step settings'),
        '#open' => TRUE,
        '#weight' => 1,
    ];

    $form['step_user_fields'] = [
        '#type' => 'details',
        '#title' => t('Step fields setting'),
        '#open' => TRUE,
        '#weight' => 2,
    ];

    $form['label'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Label'),
        '#maxlength' => 255,
        '#default_value' => $registration_step->label(),
        '#description' => $this->t("Label for the registration step."),
        '#required' => TRUE,
      '#weight' => 0,
    ];

    $form['id'] = [
        '#type' => 'machine_name',
        '#default_value' => $registration_step->id(),
        '#machine_name' => [
            'exists' => [$this, 'exist'],
        ],
        '#disabled' => !$registration_step->isNew(),
    ];

    $form['step_settings']['description'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Description'),
        '#default_value' => $registration_step->getDescription(),
        '#description' => $this->t("Description for the registration step."),
        '#required' => FALSE,
    ];

    $form['step_settings']['next_button_label'] = [
        '#type' => 'textfield',
        '#title' => t('Next button label'),
        '#default_value' => $registration_step->getNextButtonLabel(),
        '#required' => TRUE,
    ];

    $form['step_settings']['previous_button_label'] = [
        '#type' => 'textfield',
        '#title' => t('Previous button label'),
        '#default_value' => $registration_step->getPreviousButtonLabel(),
        '#required' => TRUE,
    ];

    $form['step_user_fields']['user_fields'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('User fields'),
      '#options' => _multistep_registration_fome_get_user_fields(),
      '#attributes' => [
        'class' => ['user_fields'],
      ],
      '#default_value' => $registration_step->getUserFields(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Fields used in another steps.
    $used_fields = _multistep_registration_form_get_used_fields();

    // Fields used in another steps.
    $original_user_fields = _multistep_registration_fome_get_user_fields();

    // Fields used in the current step.
    $step_fields = $form_state->getValue('user_fields');
    $step_fields = $this->filterFields($step_fields);
    $error_fields = [];

    foreach ($step_fields as $key => $field) {
      foreach ($used_fields as $step_id => $used_field) {
        if (in_array($field, $used_fields[$step_id])) {
          $step_label = $this->entityTypeManager->getStorage('registration_step')
            ->load('basic_info')->label();

          $error_fields[] = $original_user_fields[$field] . $this->t(' has been used in step ') . $step_label;
        }
      }
    }

    $error_fields = array_unique($error_fields);

    if (!empty($error_fields)) {
      $form_state->setErrorByName('user_fields', $this->t('The following field(s) have already been used in the following steps: %fields',
        ['%fields' => implode(', ', array_unique($error_fields))]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $registration_step = $this->entity;
    $status = $registration_step->save();

    // Prepare user fields.
    $user_fields = $form_state->getValue('user_fields');
    $fields = $this->filterFields($user_fields);

    // Save user fields as array to map it to sequence.
    $registration_step->set('user_fields', $fields);
    $status = $registration_step->save();

    if ($status === SAVED_NEW) {
      $this->messenger()->addMessage($this->t('The %label Registration Step created.', [
          '%label' => $registration_step->label(),
      ]));

      $form_state->setRedirect('entity.registration_step.user_fields_form', [
        'registration_step' => $registration_step->id()
      ]);
    }
    else {
      $this->messenger()->addMessage($this->t('The %label Registration Step updated.', [
          '%label' => $registration_step->label(),
      ]));
      $form_state->setRedirect('entity.multistep_registration_form.collection');
    }
  }

  /**
   * Helper function to check whether the Registration Step configuration entity exists.
   */
  public function exist($id) {
    $entity = $this->entityTypeManager->getStorage('registration_step')->getQuery()
        ->condition('id', $id)
        ->execute();
    return (bool) $entity;
  }

  public function filterFields(array $fields) {
    foreach ($fields as $key => $field) {
      if ($field == 0) {
        unset($fields[$key]);
      }
    }

    return $fields;
  }

}
